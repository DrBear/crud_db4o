﻿using System.Collections.Generic;
using DB4O_CRUD.Enums;

namespace DB4O_CRUD.Model
{
    public class Phone
    {
        public static Dictionary<NumberType, string> NumberTypes = new Dictionary<NumberType, string>
        {
            {Enums.NumberType.None, string.Empty},
            {Enums.NumberType.Stationary, "Stacjonarny"},
            {Enums.NumberType.Mobile, "Mobilny"}
        };

        public Phone(string number, string network, NumberType numberType)
        {
            Number = number;
            Network = network;
            NumberType = NumberTypes[numberType];
        }

        public Phone()
        {
            
        }
        public string Number { get; set; }
        public string Network { get; set; }
        public string NumberType { get; set; }
    }
}