﻿using System.Collections.Generic;

namespace DB4O_CRUD.Model
{
    public class Person
    {
        public Person(string firstname, string lastname)
        {
            Firstname = firstname;
            Lastname = lastname;
        }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public Address Address { get; set; }
        public List<Phone> Phones { get; set; }
    }
}