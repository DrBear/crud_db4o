﻿namespace DB4O_CRUD.Model
{
    public class Address
    {
        public Address(string street, string postalCode, string city)
        {
            Street = street;
            PostalCode = postalCode;
            City = city;
        }

        public string Street { get; set; }
        
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}