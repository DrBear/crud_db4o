﻿using System;
using System.Text;

namespace DB4O_CRUD
{
    public static class Tools
    {
        public static void DashMaker()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < Console.WindowWidth; i++)
            {
                sb.Append("_");
            }

            Console.WriteLine(sb.ToString());
        }
    }
}