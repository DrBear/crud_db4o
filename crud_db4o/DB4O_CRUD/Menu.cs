﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Cecil.FlowAnalysis.ActionFlow;
using DB4O_CRUD.DataAccessLayer;
using DB4O_CRUD.Enums;
using DB4O_CRUD.Model;

namespace DB4O_CRUD
{
    public class Menu
    {
        private string _dbName { get; }
        private readonly Db4ORepository _repo;

        public Menu()
        {
            _dbName = ChooseDbName();
            _repo = new Db4ORepository(_dbName);
            MainMenu();
        }

        private string ChooseDbName()
        {
            Console.WriteLine("Podaj nazwę bazy danych \n");
            return Console.ReadLine();
        }

        private void MainMenu()
        {
            Console.Clear();
            Console.WriteLine($"Witaj w programie obsługującym bazę {_dbName}");
            Console.WriteLine("1. Wyświetl liste osób");
            Console.WriteLine("2. Dodaj osobę");
            Console.WriteLine("3. Usuń osobę");
            Console.WriteLine("4. Edytuj osobę");
            Console.WriteLine("5. Statystyki");
            Console.Write("\nWyjście...");

            switch (Console.ReadKey().KeyChar)
            {
                case '1':
                {
                    ShowPeopleListMenu();
                    break;
                }
                case '2':
                {
                    AddPersonMenu();
                    break;
                }
                case '3':
                {
                    DeletePersonMenu();
                    break;
                }
                case '4':
                {
                    EditPersonMenu();
                    break;
                }
                case '5':
                {
                    Statistics();
                    break;
                }
                default:
                {
                    Environment.Exit(0);
                    break;
                }
            }
        }

        private void AddPersonMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Menu dodawania osób");
                Person persontoAdd;
                while (true)
                {
                    Console.WriteLine("Podaj imię \n");
                    var firstname = Console.ReadLine();
                    Console.WriteLine("Podaj nazwisko \n");
                    var lastname = Console.ReadLine();
                    persontoAdd = new Person(firstname, lastname);
                    Console.WriteLine(Environment.NewLine);
                    if (!_repo.IsPersonExistInDb(persontoAdd))
                        break;
                    Console.WriteLine("Podana osoba istnieje już w bazie");
                    Console.WriteLine("Chcesz spróbować ponownie? (t/n)");
                    if (Console.ReadKey().KeyChar == 'n')
                    {
                        MainMenu();
                    }

                    Console.Clear();
                }

                Console.WriteLine(@"Czy chcesz dodać adres? (t\n)");
                if (Console.ReadKey().KeyChar == 't')
                {
                    Console.Clear();
                    Console.WriteLine(Environment.NewLine);
                    Console.WriteLine("Podaj ulicę \n");
                    var street = Console.ReadLine();
                    Console.WriteLine("Podaj miasto \n");
                    var city = Console.ReadLine();
                    Console.WriteLine("Podaj kod pocztowy \n");
                    var postalcode = Console.ReadLine();
                    persontoAdd.Address = new Address(street, postalcode, city);
                }

                Console.WriteLine(Environment.NewLine);
                Console.WriteLine(@"Czy chcesz dodać numer telefonu? (t\n)");
                if (Console.ReadKey().KeyChar == 't')
                {
                    List<Phone> phonelist = new List<Phone>();
                    while (true)
                    {
                        Console.Clear();
                        Console.WriteLine(Environment.NewLine);
                        Console.WriteLine("Podaj numer \n");
                        var number = Console.ReadLine();
                        Console.WriteLine("Wybierz typ:\n1. Domowy\n2. Komórkowy\n");
                        var numberType = Console.ReadKey().KeyChar == '1' ? NumberType.Stationary : NumberType.Mobile;
                        Console.WriteLine("Podaj Operatora \n");
                        var network = Console.ReadLine();
                        var phone = new Phone(number, network, numberType);
                        phonelist.Add(phone);
                        Console.WriteLine(@"Czy chcesz dodać kolejny? (t\n) ",Environment.NewLine);
                        if (Console.ReadKey().KeyChar == 'n')
                        {
                            break;
                        }
                    }

                    persontoAdd.Phones = phonelist;
                    Console.WriteLine(Environment.NewLine);
                }

                _repo.AddUpdateNewPerson(persontoAdd);
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine(@"Czy chcesz dodać kolejną osobę? (t\n)");
                if (Console.ReadKey().KeyChar == 'n')
                {
                    break;
                }
            }

            MainMenu();
        }
        private void ShowPeopleListMenu()
        {
            Console.Clear();
            Console.WriteLine("Menu dodawania osób \n");
            Console.WriteLine("Jaką listę chcesz wyświetlić? \nProstą (1)\nSzczegółową (2)");
            switch (Console.ReadKey().KeyChar)
            {
                case '1':
                {
                    Console.Clear();
                    Console.WriteLine("Prosta lista osób w bazie \n\n");
                    Tools.DashMaker();
                    if (_repo.GetAll().Any())
                    {
                        foreach (var person in _repo.GetAll())
                        {
                            Console.WriteLine($"Imię:   {person?.Firstname} Nazwisko: {person?.Lastname}");
                            Tools.DashMaker();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Brak osób w bazie");
                    }

                    Console.WriteLine(Environment.NewLine);
                    Console.WriteLine("Kliknij enter aby wyjść do menu głównego");
                    Console.ReadLine();
                    break;
                }
                case '2':
                {
                    Console.Clear();
                    Console.WriteLine("Szczegółowa lista osób w bazie");
                    if (_repo.GetAll().Any())
                    {
                        Tools.DashMaker();
                        foreach (var person in _repo.GetAll())
                        {
                            Console.WriteLine($"Imię: {person?.Firstname}   Nazwisko: {person?.Lastname}");
                            if (person?.Address != null)
                            {
                                Console.WriteLine(
                                    $"Miasto: {person.Address.City}   Ulica: {person.Address.Street}  Kod pocztowy: {person.Address.PostalCode} \n");
                            }
                            else
                                Console.WriteLine("Brak adresu");

                            if (person.Phones != null && person.Phones.Any())
                            {
                                foreach (var phone in person.Phones)
                                {
                                    Console.WriteLine(
                                        $"Numer telefonu: {phone.Number}  Operator: {phone.Network}   Typ: {phone.NumberType}");
                                }
                            }
                            else
                                Console.WriteLine("Brak numerów");

                            Tools.DashMaker();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Brak osób w bazie");
                    }

                    Console.WriteLine(Environment.NewLine);
                    Console.WriteLine("Kliknij enter aby wyjść do menu głównego");
                    Console.ReadLine();
                    break;
                }

            }

            MainMenu();
        }
        private void DeletePersonMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Menu usuwania osób \n");
                Console.WriteLine("Podaj imię osoby do usunięcia");
                var firstname = Console.ReadLine();
                Console.WriteLine("Podaj nazwisko osoby do usunięcia");
                var lastname = Console.ReadLine();

                if (_repo.IsPersonExistInDb(new Person(firstname, lastname)))
                {
                    Console.Clear();
                    Console.WriteLine($"Jesteś pewien, że chcesz usunąć {firstname} {lastname}? (t/n)",
                        Environment.NewLine);
                    if (Console.ReadKey().KeyChar == 't')
                    {
                        Console.WriteLine(Environment.NewLine);
                        _repo.DeletePersonByNames(firstname, lastname);
                        Console.WriteLine($"{firstname} {lastname} został usunięty");
                        Console.WriteLine("Kliknij enter aby wrócić do menu ...");
                        Console.ReadLine();
                        MainMenu();
                    }
                    else
                    {
                        MainMenu();
                    }
                }
                else
                {
                    Console.WriteLine($"{firstname} {lastname} nie istnieje w bazie");
                    Console.WriteLine("Chcesz spróbować ponownie? (t/n)");
                    if (Console.ReadKey().KeyChar == 'n')
                    {
                        MainMenu();
                    }
                }
            }
        }

        private void EditPersonMenu()
        {
            while (true)
            {
                Console.Clear();
                string firstname;
                string lastname;
                Person persontoupdate;
                Console.WriteLine("Menu edytowania osób \n");
                while (true)
                {
                    Console.WriteLine("Podaj imię \n");
                    firstname = Console.ReadLine();
                    Console.WriteLine("Podaj nazwisko \n");
                    lastname = Console.ReadLine();
                    if (_repo.IsPersonExistInDb(firstname,lastname))
                    {
                        persontoupdate = _repo.GetPersonByName(firstname, lastname);
                        break;
                    }
                    Console.WriteLine("Podana osoba nie istnieje w bazie\nCzy chcesz spróbować ponownie?(t/n)");
                    if (Console.ReadKey().KeyChar=='n')
                    {
                        MainMenu();
                    }
                }
                Console.Clear();
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine($"Wybrana osoba {firstname} {lastname}");
                    Console.WriteLine("Wybierz co chcesz edytować \n1.Imię\n2.Nazwisko\n3.Adres\n4.Listę telefonów\nKliknij enter aby powrócić do menu głównego\n");
                    switch (Console.ReadKey().KeyChar)
                    {
                        case '1':
                            Console.Clear();
                            Console.WriteLine("Edycja imienia");
                            Console.WriteLine($"Aktualna wartość to {firstname}\n");
                            Console.WriteLine("Podaj nowe imię");
                            var newfirstname = Console.ReadLine();
                            Console.WriteLine($"Czy chcesz zmienić {firstname} na {newfirstname} (t/n)");
                            if (Console.ReadKey().KeyChar=='t')
                            {
                                persontoupdate.Firstname = newfirstname;
                                _repo.AddUpdateNewPerson(persontoupdate);
                                Console.Clear();
                                Console.WriteLine($"Imię wybranej osoby {firstname} {lastname} zostało zmienione na {newfirstname}");
                                Console.WriteLine("Czy chcesz wrócić do menu edycji? (t/n)");
                                if (Console.ReadKey().KeyChar=='n')
                                {
                                    MainMenu();
                                }
                                    
                            }
                            break;
                        case '2':
                            Console.Clear();
                            Console.WriteLine("Edycja nazwiska");
                            Console.WriteLine($"Aktualna wartość to {lastname}\n");
                            Console.WriteLine("Podaj nowe nazwisko");
                            var newlastname = Console.ReadLine();
                            Console.WriteLine($"Czy chcesz zmienić {lastname} na {newlastname} (t/n)");
                            if (Console.ReadKey().KeyChar == 't')
                            {
                                persontoupdate.Lastname = newlastname;
                                _repo.AddUpdateNewPerson(persontoupdate);
                                Console.Clear();
                                Console.WriteLine($"Nazwisko wybranej osoby {firstname} {lastname} zostało zmienione na {newlastname}");
                                Console.WriteLine("Czy chcesz wrócić do menu edycji? (t/n)");
                                if (Console.ReadKey().KeyChar == 'n')
                                {
                                    MainMenu();
                                }
                                    
                            }
                            break;
                        case '3':
                            if (persontoupdate.Address != null)
                            {
                                Console.Clear();
                                Console.WriteLine($"Aktualna wartość to {persontoupdate.Address?.Street} {persontoupdate.Address?.City} {persontoupdate.Address?.PostalCode}\n");
                                while (true)
                                {
                                    Console.WriteLine("Menu akcji edycji adresu\n1.Usunięcie adresu\n2.Edycja Adresu\n3.Powrót");
                                    var choose = Console.ReadKey().KeyChar;
                                    if (choose == '1')
                                    {
                                        Console.WriteLine("Czy jesteś pewien, że chcesz usunąć adres? (t/n)");
                                        if (Console.ReadKey().KeyChar == 't')
                                        {
                                            Console.WriteLine(Environment.NewLine);
                                            _repo.DeleteAddress(persontoupdate);
                                            persontoupdate.Address = null;
                                            _repo.AddUpdateNewPerson(persontoupdate);
                                            Console.WriteLine("Adres został usunięty\nCzy chcesz wrócić do menu głównego? (t/n)");
                                            if (Console.ReadKey().KeyChar=='t')
                                            {
                                                MainMenu();
                                            }
                                        }

                                    }

                                    else if (choose == '2')
                                    {
                                        Console.Clear();
                                        Console.WriteLine($"Aktualna wartość to {persontoupdate.Address?.Street} {persontoupdate.Address?.City} {persontoupdate.Address?.PostalCode}\n");
                                        Console.WriteLine("\nCzy chcesz edytować ulice? t/n");
                                        if (Console.ReadKey().KeyChar == 't')
                                        {
                                            Console.WriteLine(Environment.NewLine);
                                            Console.WriteLine("Podaj nowa ulice: ");
                                            persontoupdate.Address.Street = Console.ReadLine();
                                        }

                                        Console.WriteLine("\nCzy chcesz edytować miasto? t/n");
                                        if (Console.ReadKey().KeyChar == 't')
                                        {
                                            Console.WriteLine(Environment.NewLine);
                                            Console.WriteLine("Podaj nowe miasto: ");
                                            persontoupdate.Address.City = Console.ReadLine();
                                        }

                                        Console.WriteLine("\nCzy chcesz edytować kod pocztowy? t/n");
                                        if (Console.ReadKey().KeyChar == 't')
                                        {
                                            Console.WriteLine(Environment.NewLine);
                                            Console.WriteLine("Podaj nowy kod pocztowy: ");
                                            persontoupdate.Address.PostalCode = Console.ReadLine();
                                        }
                                            
                                        _repo.AddUpdateNewPerson(persontoupdate);
                                        Console.WriteLine("Osoba została pomyślnie zedytowana");
                                        Console.Clear();
                                    }

                                    else
                                    {
                                        break;
                                    }
                                    break;
                                }
                            }
                            Console.Clear();
                            Console.WriteLine("Wybrana osoba nie posiada adresu");
                            Console.WriteLine("Naciśnij enter aby powrócić do menu...");
                            Console.ReadLine();
                            break;
                        case '4':
                            while (true)
                            {
                                if (persontoupdate.Phones!= null && persontoupdate.Phones.Any())
                                {
                                    Console.Clear();
                                    Console.WriteLine($"Lista telefonów {firstname} {lastname}");
                                    Tools.DashMaker();
                                    var index = 0;
                                    foreach (var phone in persontoupdate.Phones)
                                    {
                                        Console.WriteLine($"{index++}:   {phone.Number}  {phone.Network} {phone.NumberType}");
                                        Tools.DashMaker();
                                    }

                                    Console.WriteLine("Wybierz indeks numeru do edycji lub naciśnij enter aby powrócić");
                                    var isParsable = int.TryParse(Console.ReadLine(), out var choosedindex);
                                    if (isParsable)
                                    {
                                        Console.Clear();
                                        Console.WriteLine($"Edytowany numer {persontoupdate.Phones[choosedindex].Number}    {persontoupdate.Phones[choosedindex].Network}    {persontoupdate.Phones[choosedindex].NumberType}");
                                        while (true)
                                        {
                                            Console.WriteLine("Wybierz akcję\n1.Usuń numer\n2.Edytuj\n3.Powrót\n");
                                            var key = Console.ReadKey().KeyChar;
                                            if (key=='1')
                                            {
                                                Console.WriteLine(Environment.NewLine);
                                                Console.WriteLine($"Czy jesteś pewien, że chcesz usunąć {persontoupdate.Phones[choosedindex].Number}    {persontoupdate.Phones[choosedindex].Network}    {persontoupdate.Phones[choosedindex].NumberType} (t/n) ");
                                                if (Console.ReadKey().KeyChar == 't')
                                                {
                                                    Console.WriteLine(Environment.NewLine);
                                                    _repo.DeletePhoneNumber(new List<int>(){choosedindex},persontoupdate);
                                                    Console.WriteLine($"Numer {persontoupdate.Phones[choosedindex]} został usunięty");
                                                    persontoupdate.Phones.Remove(persontoupdate.Phones[choosedindex]);
                                                    _repo.AddUpdateNewPerson(persontoupdate);
                                                    Console.WriteLine("Naciśnij enter aby powrócić do menu edycji telefonów");
                                                    Console.ReadLine();
                                                }
                                                break;
                                            }
                                            else if(key == '2')
                                            {
                                                bool edited = false;
                                                Console.Clear();
                                                Console.WriteLine($"Edytowany numer {persontoupdate.Phones[choosedindex].Number}    {persontoupdate.Phones[choosedindex].Network}    {persontoupdate.Phones[choosedindex].NumberType}");
                                                Console.WriteLine("Czy chcesz edytować numer? (t/n) \n");
                                                if (Console.ReadKey().KeyChar=='t')
                                                {
                                                    Console.WriteLine(Environment.NewLine);
                                                    Console.WriteLine("Podaj nowy numer telefonu");
                                                    var newnumber = Console.ReadLine();
                                                    persontoupdate.Phones
                                                            .FirstOrDefault(x => x == persontoupdate.Phones[choosedindex])
                                                            .Number =
                                                        newnumber;
                                                    edited = true;
                                                }
                                                Console.WriteLine("Czy chcesz edytować operatora? (t/n) \n");
                                                if (Console.ReadKey().KeyChar == 't')
                                                {
                                                    Console.WriteLine(Environment.NewLine);
                                                    Console.WriteLine("Podaj nowego operatora");
                                                    var newoperator = Console.ReadLine();
                                                    persontoupdate.Phones
                                                            .FirstOrDefault(x => x == persontoupdate.Phones[choosedindex])
                                                            .Network =
                                                        newoperator;
                                                    edited = true;
                                                }
                                                Console.WriteLine("Czy chcesz edytować typ numeru? (t/n) \n");
                                                if (Console.ReadKey().KeyChar == 't')
                                                {
                                                    Console.WriteLine(Environment.NewLine);
                                                    Console.WriteLine("Wybierz typ:\n1. Domowy\n2. Komórkowy\n");
                                                    var newnumberType = Console.ReadKey().KeyChar == '1' ? NumberType.Stationary : NumberType.Mobile;
                                                    persontoupdate.Phones
                                                            .FirstOrDefault(x => x == persontoupdate.Phones[choosedindex])
                                                            .NumberType =
                                                        Phone.NumberTypes[newnumberType];
                                                    edited = true;
                                                }
                                                if (edited)
                                                {
                                                    _repo.AddUpdateNewPerson(persontoupdate);
                                                    Console.WriteLine("Podany numer został edytowany");
                                                    Console.WriteLine("Naciśnij enter aby powrócić do menu edycji...");
                                                    Console.ReadLine();
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                                else
                                {
                                    Console.Clear();
                                    Console.WriteLine("Podana osoba nie posiada numerów telefonu");
                                    Console.WriteLine("Naciśnij enter aby powrócić do menu...");
                                    Console.ReadLine();
                                    break;
                                }
                            }
                            break;
                        case '5':
                            break;
                        default:
                            MainMenu();
                            break;
                    }
                }
            }

        }
        private void Statistics()
        {
            Console.Clear();
            var statlist = _repo.Statistic();
            Console.WriteLine("Statystyki\n\n");
            Console.WriteLine($"Ilość osób: {statlist["Peoples"]}\n");
            Console.WriteLine($"Ilość adresów: {statlist["Addresses"]}\n");
            Console.WriteLine($"Ilość numerów telefonu: {statlist["Phones"]}\n");
            Console.Write("\nPowrot...");
            Console.ReadKey();
            MainMenu();
        }

    }
}
