﻿namespace DB4O_CRUD.Enums
{
    public enum NumberType
    {
        None,
        Stationary,
        Mobile
    }
}