﻿using System.Collections.Generic;
using System.Linq;
using Db4objects.Db4o;
using Db4objects.Db4o.Linq;
using DB4O_CRUD.Model;

namespace DB4O_CRUD.DataAccessLayer
{
    public class Db4ORepository
    {
        private string _dbname { get; }

        private static IObjectContainer _db { get; set; } 
        public Db4ORepository(string dbname)
        {
            _dbname = dbname;
            var config = Db4oEmbedded.NewConfiguration();
            config.Common.UpdateDepth = 5;
            config.Common.ObjectClass(typeof(Person)).CascadeOnUpdate(true);
            config.Common.ObjectClass(typeof(Person)).CascadeOnDelete(true);
            _db = Db4oEmbedded.OpenFile(config, _dbname);
        }

        public void AddUpdateNewPerson(Person personToAddUpdate)
        {
            try
            {
                _db.Ext().Store(personToAddUpdate, 5);
            }
            finally
            {
                _db.Commit();
            }
        }
        public void DeletePerson(Person personToDelete)
        {
            try
            {
                _db.Delete(personToDelete);
            }
            finally
            {
                _db.Commit();
            }
        }
        public void DeletePersonByNames(string firstname, string lastname)
        {
            try
            {
                _db.Delete(_db.AsQueryable<Person>().FirstOrDefault(x=>x.Firstname == firstname && x.Lastname==lastname));
            }
            finally
            {
                _db.Commit();
            }
        }

        public bool IsPersonExistInDb(Person persontocheck)
        {
            if (_db.Query<Person>(x=>x.Firstname == persontocheck.Firstname && x.Lastname == persontocheck.Lastname).Any())
                return true;
            else
            return false;
        }
        public bool IsPersonExistInDb(string firstname, string lastname)
        {
            if (_db.Query<Person>(x => x.Firstname == firstname && x.Lastname == lastname).Any())
                return true;
            else
                return false;
        }

        public void DeleteAddress(Person person)
        {
            try
            {
                _db.Delete(_db.Query<Address>(x => x == person.Address).FirstOrDefault());
            }
            finally

            {
                _db.Commit();
            }
        }

        public void DeletePhoneNumber(List<int> indexes, Person person)
        {
            foreach (var phone in indexes)
            {
                try
                {
                    _db.Delete(_db.Query<Phone>(x => x.Number == person.Phones[phone].Number).FirstOrDefault());
                }
                finally

                {
                    _db.Commit();
                }
            }
        }
        public Dictionary<string,int> Statistic()
        {
            return new Dictionary<string, int>
            {
                {"Peoples",_db.Query<Person>().Count},
                {"Addresses",_db.Query<Address>().Count},
                {"Phones",_db.Query<Phone>().Count}
            };
        }

        public Person GetPersonByName(string firstname, string lastname)
        {
            return _db.Query<Person>(x => x.Firstname == firstname && x.Lastname == lastname).FirstOrDefault();
        }
        public IQueryable<Person> GetAll()
        {
            return new List<Person>(_db.Query<Person>()).AsQueryable();
        }
    }
}